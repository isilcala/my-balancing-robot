#include "RobotWifi.h"

RobotPreference preference;

RobotWifi::RobotWifi()
{
}

bool RobotWifi::connectToLastKnown()
{
    bool connected = false;
    WiFi.mode(WIFI_STA);

    /* 
        Try to connect to the Wifi stored in EEPROM.
        */
    char ssid[63];
    char pwd[63];
    preference.getBytes(KEY_WIFI_SSID, ssid, 63);
    preference.getBytes(KEY_WIFI_PASSWORD, pwd, 63);
    Serial << "Connecting to '" << ssid << "' stored in EEPROM." << endl;
    WiFi.begin(ssid, pwd);
    if (!(WiFi.waitForConnectResult() != WL_CONNECTED))
    {
        Serial << "Connected to '" << ssid << "' with IP address: " << WiFi.localIP() << endl;
        connected = true;

        return connected;
    }

    /* 
        Failed to connect to the Wifi stored in EEPROM.
        Now try to connect to the Wifi specified in code. 
        */
    Serial << "Connecting to '" << WIFI_SSID << "' specified in code." << endl;
    WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
    if (!(WiFi.waitForConnectResult() != WL_CONNECTED))
    {
        Serial << "Connected to '" << WIFI_SSID << "' with IP address: " << WiFi.localIP() << endl;
        connected = true;
        preference.putBytes(KEY_WIFI_SSID, WIFI_SSID, 63);
        preference.putBytes(KEY_WIFI_PASSWORD, WIFI_PASSWORD, 63);
    }
    else
    {
        Serial << "Could not connect to Wifi" << endl;
        connected = false;
    }

    return connected;
}

bool RobotWifi::robotAP()
{
    bool started = false;
    Serial << "Starting AP with SSID '" << AP_SSID << "'" << endl;
    WiFi.mode(WIFI_AP_STA);
    // WiFi.softAPConfig(apIP, apIP, IPAddress(192,168,178,24));
    started = WiFi.softAP(AP_SSID, AP_PASSWORD);
    Serial << "AP started with IP address: " << WiFi.softAPIP() << endl;

    return started;
}