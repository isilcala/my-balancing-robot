#ifndef _ROBOTPREFERENCE_H_
#define _ROBOTPREFERENCE_H_

#include <Preferences.h>
#include <Streaming.h>

// Bump version number up when the structure of settings changes
#define VERSION_NO 1

#define SETTING_NAME "settings"
#define KEY_VERSION "pref_ver"

class RobotPreference : public Preferences
{
public:
    RobotPreference();
    void setup(void);
};

extern RobotPreference preference;

#endif /* _ROBOTPREFERENCE_H_ */