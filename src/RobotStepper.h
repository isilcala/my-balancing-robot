#ifndef _ROBOTSTEPPER_H_
#define _ROBOTSTEPPER_H_

#include <Arduino.h>
#include <SPI.h>
#include <TMCStepper.h>
#include <HardwareSerial.h>
#include <FastAccelStepper.h>

#include "pin-assignment.h"
#include "variables.h"

class RobotStepper
{
public:
    RobotStepper();
    void setup();
    void enableMotors();
    void disableMotors();
    void leftAccel(int32_t accel);
    int leftSpeed(int32_t speed);
    int rightSpeed(int32_t speed);
private:
};

#endif /* _ROBOTSTEPPER_H_ */
