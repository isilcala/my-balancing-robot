#include "RobotPreference.h"
#include "variables.h"

RobotPreference::RobotPreference()
{
}

void RobotPreference::setup(void)
{
    // Serial << "============ Preference ============" << endl;
    begin(SETTING_NAME, false); // false = RW-mode

    // Check version
    if (getUInt(KEY_VERSION, 0) != VERSION_NO)
    {
        clear(); // Delete all preferences under the opened namespace
        putUInt(KEY_VERSION, VERSION_NO);
        Serial << "EEPROM version not match, all preference settings are deleted." << endl;
    }

    Serial << "Preference initialized. Current version: " << VERSION_NO << endl;

    // Serial << "Preference Variables:" << endl;
    // Serial << PK_ANGLE_SETPOINT << ": " << preference.getFloat("ANGLE_SETPOINT") << endl;
}