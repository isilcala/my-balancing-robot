#ifndef _ROBOTWIFI_H_
#define _ROBOTWIFI_H_

#include <WiFi.h>
#include "wifi-config.h"
#include "RobotPreference.h"

#define KEY_WIFI_MODE "wifi_mode"
#define KEY_WIFI_SSID "wifi_ssid"
#define KEY_WIFI_PASSWORD "wifi_pass"

const char host[] = "MyRobot";

class RobotWifi
{
public:
    RobotWifi();
    bool connectToLastKnown();
    bool robotAP();
};

#endif /* _ROBOTWIFI_H_ */
