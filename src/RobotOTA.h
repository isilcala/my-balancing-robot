#ifndef _ROBOTOTA_H_
#define _ROBOTOTA_H_
#include <ArduinoOTA.h>
#include "variables.h"
#include "pin-assignment.h"

class RobotOTA
{
public:
    RobotOTA();
    void setup();
    void handle();
};

#endif /* _ROBOTOTA_H_ */