#include "RobotStepper.h"

// TMC2208Stepper driver(&SERIAL_PORT, R_SENSE);                     // Hardware Serial
//TMC2208Stepper driver(SW_RX, SW_TX, R_SENSE);                     // Software serial
// TMC2209Stepper driver(&serial2, R_SENSE, DRIVER_ADDRESS);
//TMC2209Stepper driver(SW_RX, SW_TX, R_SENSE, DRIVER_ADDRESS);
HardwareSerial stepperSerial(STEPPER_HW_UART);
TMC2209Stepper _leftDriver(&stepperSerial, R_SENSE, LEFT_DRIVER_ADDRESS);
TMC2209Stepper _rightDriver(&stepperSerial, R_SENSE, RIGHT_DRIVER_ADDRESS);
FastAccelStepperEngine _stepperEngine = FastAccelStepperEngine();
FastAccelStepper *_leftStepper = NULL;
FastAccelStepper *_rightStepper = NULL;

RobotStepper::RobotStepper()
{
}

void RobotStepper::setup()
{
    /* Baud-rates available: 300, 600, 1200, 2400, 4800, 9600, 14400, 19200, 28800, 38400, 57600, or 115200, 256000, 512000, 962100
 *
 *  Protocols available:
 * SERIAL_5N1   5-bit No parity 1 stop bit
 * SERIAL_6N1   6-bit No parity 1 stop bit
 * SERIAL_7N1   7-bit No parity 1 stop bit
 * SERIAL_8N1 (the default) 8-bit No parity 1 stop bit
 * SERIAL_5N2   5-bit No parity 2 stop bits
 * SERIAL_6N2   6-bit No parity 2 stop bits
 * SERIAL_7N2   7-bit No parity 2 stop bits
 * SERIAL_8N2   8-bit No parity 2 stop bits
 * SERIAL_5E1   5-bit Even parity 1 stop bit
 * SERIAL_6E1   6-bit Even parity 1 stop bit
 * SERIAL_7E1   7-bit Even parity 1 stop bit
 * SERIAL_8E1   8-bit Even parity 1 stop bit
 * SERIAL_5E2   5-bit Even parity 2 stop bit
 * SERIAL_6E2   6-bit Even parity 2 stop bit
 * SERIAL_7E2   7-bit Even parity 2 stop bit
 * SERIAL_8E2   8-bit Even parity 2 stop bit
 * SERIAL_5O1   5-bit Odd  parity 1 stop bit
 * SERIAL_6O1   6-bit Odd  parity 1 stop bit
 * SERIAL_7O1   7-bit Odd  parity 1 stop bit
 * SERIAL_8O1   8-bit Odd  parity 1 stop bit
 * SERIAL_5O2   5-bit Odd  parity 2 stop bit
 * SERIAL_6O2   6-bit Odd  parity 2 stop bit
 * SERIAL_7O2   7-bit Odd  parity 2 stop bit
 * SERIAL_8O2   8-bit Odd  parity 2 stop bit
*/
    // serial2.begin(115200, SERIAL_8N1, RX2, TX2);

    pinMode(STEP_EN_PIN, OUTPUT);
    pinMode(LEFT_STEP_PIN, OUTPUT);
    pinMode(LEFT_DIR_PIN, OUTPUT);
    pinMode(RIGHT_STEP_PIN, OUTPUT);
    pinMode(RIGHT_DIR_PIN, OUTPUT);

    digitalWrite(STEP_EN_PIN, HIGH); // disable driver during initialization

    _stepperEngine.init();

    stepperSerial.begin(STEPPER_UART_BAUDRATE);
    _leftDriver.begin();                        //  SPI: Init CS pins and possible SW SPI pins
                                                // UART: Init SW UART (if selected) with default 115200 baudrate
    _leftDriver.toff(5);                        // Enables driver in software
    _leftDriver.rms_current(MOTOR_RMS_CURRENT); // Set motor RMS current

    //driver.en_pwm_mode(true);       // Toggle stealthChop on TMC2130/2160/5130/5160
    //driver.en_spreadCycle(false);   // Toggle spreadCycle on TMC2208/2209/2224
    _leftDriver.pwm_autoscale(true); // Needed for stealthChop
    // _leftDriver.shaft(LEFT_FORWARD_DIR);
    _leftDriver.microsteps(STEPPER_MICROSTEPS);

    _leftStepper = _stepperEngine.stepperConnectToPin(LEFT_STEP_PIN);
    _leftStepper->setEnablePin(STEP_EN_PIN);
    _leftStepper->setDirectionPin(LEFT_DIR_PIN, LEFT_FORWARD_DIR);
    _leftStepper->setDelayToEnable(STEPPER_DELAY_TO_ENABLE);
    _leftStepper->setSpeedInHz(4000);
    _leftStepper->setAcceleration(STEPPER_ACCELERATION); // value range: 1 ~ 4294967295

    _rightDriver.begin();                        //  SPI: Init CS pins and possible SW SPI pins
                                                // UART: Init SW UART (if selected) with default 115200 baudrate
    _rightDriver.toff(5);                        // Enables driver in software
    _rightDriver.rms_current(MOTOR_RMS_CURRENT); // Set motor RMS current

    //driver.en_pwm_mode(true);       // Toggle stealthChop on TMC2130/2160/5130/5160
    //driver.en_spreadCycle(false);   // Toggle spreadCycle on TMC2208/2209/2224
    _rightDriver.pwm_autoscale(true); // Needed for stealthChop
    // _leftDriver.shaft(LEFT_FORWARD_DIR);
    _rightDriver.microsteps(STEPPER_MICROSTEPS);

    _rightStepper = _stepperEngine.stepperConnectToPin(RIGHT_STEP_PIN);
    _rightStepper->setEnablePin(STEP_EN_PIN);
    _rightStepper->setDirectionPin(RIGHT_DIR_PIN, RIGHT_FORWARD_DIR);
    _rightStepper->setDelayToEnable(STEPPER_DELAY_TO_ENABLE);
    _rightStepper->setSpeedInHz(4000);
    _rightStepper->setAcceleration(STEPPER_ACCELERATION); // value range: 1 ~ 4294967295
}

void RobotStepper::enableMotors()
{
    _leftStepper->enableOutputs();
    _rightStepper->enableOutputs();
}

void RobotStepper::disableMotors()
{
    _leftStepper->disableOutputs();
    _rightStepper->disableOutputs();
}

void RobotStepper::leftAccel(int32_t accel)
{
    // moveByAcceleration() can be called, if only the speed of the stepper
    // is of interest and that speed to be controlled by acceleration.
    // The maximum speed (in both directions) to be set by setSpeedInUs() before.
    // The behaviour will be:
    //	acceleration > 0  => accelerate towards positive maximum speed
    //	acceleration = 0  => keep current speed
    //	acceleration < 0
    //		=> accelerate towards negative maximum speed if allow_reverse
    //		=> decelerate towards motor stop if allow_reverse = false
    // return value as with move/moveTo
    _leftStepper->moveByAcceleration(accel, true);
}

int RobotStepper::leftSpeed(int32_t speed)
{
    uint32_t absSpeed = abs(speed);
    absSpeed = absSpeed > STEPPER_MAX_SPEED ? STEPPER_MAX_SPEED : absSpeed;
    _leftStepper->setSpeedInHz(absSpeed);
    return speed > 0 ? _leftStepper->runForward() : _leftStepper->runBackward();
}

int RobotStepper::rightSpeed(int32_t speed)
{
    uint32_t absSpeed = abs(speed);
    absSpeed = absSpeed > STEPPER_MAX_SPEED ? STEPPER_MAX_SPEED : absSpeed;
    _rightStepper->setSpeedInHz(absSpeed);
    return speed > 0 ? _rightStepper->runForward() : _rightStepper->runBackward();
}