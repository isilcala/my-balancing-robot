#ifndef _PIN_ASSIGNMENT_H_
#define _PIN_ASSIGNMENT_H_

/* ------ Serial Ports ------*/
// #define RX2 16
// #define TX2 17

/* ------ Stepper Drivers ------*/
#define STEP_EN_PIN         33   // Enable
#define LEFT_STEP_PIN       13
#define LEFT_DIR_PIN        12
#define RIGHT_STEP_PIN      27
#define RIGHT_DIR_PIN       14

/* ------ MPU ------*/
#define MPU_INTERRUPT_PIN 35 // use pin 2 on Arduino Uno & most boards

/* ------ Misc ------*/
#define BATTERY_VOLTAGE_PIN 39  // Battery voltage sensing with a 1.2K/(1.2K + 8.2K) divider.
#define LED_PIN 2       // (Arduino is 13, Teensy is 11, Teensy++ is 6)
#define BUTTON1_PIN 34
#define BUTTON2_PIN 36

#endif /* _PIN_ASSIGNMENT_H_ */
