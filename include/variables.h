#ifndef _VARIABLES_H_
#define _VARIABLES_H_

#define OTA_HOSTNAME "myrobot"

/* ------ Stepper Drivers ------*/
#define STEPPER_HW_UART 2       // Number of hardware serial, on esp32 usually the only avaialble one is number 2.
#define STEPPER_UART_BAUDRATE   115200      // should be < 500K
#define LEFT_DRIVER_ADDRESS 0b00 // TMC2209 Driver address according to MS1 and MS2
#define RIGHT_DRIVER_ADDRESS 0b01 // TMC2209 Driver address according to MS1 and MS2
#define R_SENSE 0.11f // Match to your driver
                      // SilentStepStick series use 0.11
                      // UltiMachine Einsy and Archim2 boards use 0.2
                      // Panucatt BSD2660 uses 0.1
                      // Watterott TMC5160 uses 0.075

#define MOTOR_RMS_CURRENT   500     // in milli-amps

#define LEFT_FORWARD_DIR    true        // flip this value if you need to flip motor direction
#define RIGHT_FORWARD_DIR    true       // flip this value if you need to flip motor direction

#define STEPPER_ACCELERATION    4000000
#define STEPPER_MICROSTEPS      16
#define STEPPER_DELAY_TO_ENABLE 50      // in ms
#define STEPPER_MAX_SPEED       4000    // steps per second

/* ------ MPU ------*/
#define BALANCING_AXIS  1       // for GY-521 module, 1 for pitch or Y axis, 2 for roll or X axis
                                //           X Accel  Y Accel  Z Accel   X Gyro   Y Gyro   Z Gyro
                                //OFFSETS    -4029,     438,    1547,      78,      45,      24
#define OFFSET_ACCEL_X -4029
#define OFFSET_ACCEL_Y 438
#define OFFSET_ACCEL_Z 1547
#define OFFSET_GYRO_X 78
#define OFFSET_GYRO_Y 45
#define OFFSET_GYRO_Z 24


/* ------ PID ------*/
#define PID_SAMPLE_TIME 5       // in ms, 1000 / 5 = 200 Hz

/* ------ Preference Keys ------*/
#define PK_ANGLE_SETPOINT   "ANGLE_SETPOINT"


/* ------ Misc ------*/
#define BATTERY_DIVIDER_FACTOR  0.12766     // 1.2 / (1.2 + 8.2)


#endif /* _VARIABLES_H_ */