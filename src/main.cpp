#include <Arduino.h>
#include <SPI.h>
#include <Wire.h>
#include <ESPmDNS.h>
#include <PID_v1.h>
#include "variables.h"
#include "pin-assignment.h"
#include "RobotPreference.h"
#include "RobotWifi.h"
#include "RobotOTA.h"
#include "RobotStepper.h"

// I2Cdev and MPU6050 must be installed as libraries, or else the .cpp/.h files
// for both classes must be in the include path of your project
#include "I2Cdev.h"

#include "MPU6050_6Axis_MotionApps_V6_12.h"
//#include "MPU6050.h" // not necessary if using MotionApps include file

// Arduino Wire library is required if I2Cdev I2CDEV_ARDUINO_WIRE implementation
// is used in I2Cdev.h
#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
#include "Wire.h"
#endif

RobotWifi wifi;
RobotOTA ota;
RobotStepper stepper;

enum OperatingMode : byte
{
  STANDING_BY, // Lying on the ground
  SELF_BALACING,
  REMOTE_CONTROLLING,
};

// struct Pid
// {
//   float kp, ki, kd;
//   float Setpoint, Input, Output;
//   PID pid;
// };

volatile OperatingMode operatingMode = STANDING_BY;

// class default I2C address is 0x68
// specific I2C addresses may be passed as a parameter here
// AD0 low = 0x68 (default for SparkFun breakout and InvenSense evaluation board)
// AD0 high = 0x69
MPU6050 mpu;
//MPU6050 mpu(0x69); // <-- use for AD0 high

// MPU control/status vars
bool dmpReady = false;  // set true if DMP init was successful
uint8_t mpuIntStatus;   // holds actual interrupt status byte from MPU
uint8_t devStatus;      // return status after each device operation (0 = success, !0 = error)
uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[64]; // FIFO storage buffer

// orientation/motion vars
Quaternion q;        // [w, x, y, z]         quaternion container
VectorInt16 aa;      // [x, y, z]            accel sensor measurements
VectorInt16 gy;      // [x, y, z]            gyro sensor measurements
VectorInt16 aaReal;  // [x, y, z]            gravity-free accel sensor measurements
VectorInt16 aaWorld; // [x, y, z]            world-frame accel sensor measurements
VectorFloat gravity; // [x, y, z]            gravity vector
float euler[3];      // [psi, theta, phi]    Euler angle container
float ypr[3];        // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector

// ================================================================
// ===               INTERRUPT DETECTION ROUTINE                ===
// ================================================================

volatile bool mpuInterrupt = false; // indicates whether MPU interrupt pin has gone high
void dmpDataReady()
{
  mpuInterrupt = true;
}

volatile unsigned long lastmill = 0;

//Define Variables we'll be connecting to
float angleSetpoint, angleInput, angleOutput;

//Specify the links and initial tuning parameters
float Kp = 100, Ki = 1, Kd = 10;
PID pidAngle(&angleInput, &angleOutput, &angleSetpoint, Kp, Ki, Kd, DIRECT);

volatile float batteryVoltage = 0;
volatile unsigned long lastBatteryMill = 0;

void wirelessTask(void *parameters)
{
  while (true)
  {
    if (operatingMode == STANDING_BY)
    {
      ota.handle();
    }

    // check battery voltage every 2 seconds
    if (millis() - lastBatteryMill > 2000)
    {
      // Battery voltage sensing with a 1.2K/(1.2K + 8.2K) divider.
      // ESP32 reads 0 to 4095 from 0.1v to 3.2v
      batteryVoltage = analogRead(BATTERY_VOLTAGE_PIN) * 3.3 / 4095.0 / BATTERY_DIVIDER_FACTOR;
      lastBatteryMill = millis();
      // Serial << "Battery Voltage: " << batteryVoltage << endl;
    }
    delay(1);
  }
}

void mpuUpdate()
{
  // if programming failed, don't try to do anything
  if (!dmpReady)
    return;

  // wait for MPU interrupt or extra packet(s) available
  while (!mpuInterrupt && fifoCount < packetSize)
  {
    if (mpuInterrupt && fifoCount < packetSize)
    {
      // try to get out of the infinite loop
      fifoCount = mpu.getFIFOCount();
    }
    // other program behavior stuff here
    // .
    // .
    // .
    // if you are really paranoid you can frequently test in between other
    // stuff to see if mpuInterrupt is true, and if so, "break;" from the
    // while() loop to immediately process the MPU data
    // .
    // .
    // .
  }

  // reset interrupt flag and get INT_STATUS byte
  mpuInterrupt = false;
  mpuIntStatus = mpu.getIntStatus();

  // get current FIFO count
  fifoCount = mpu.getFIFOCount();

  // check for overflow (this should never happen unless our code is too inefficient)
  if ((mpuIntStatus & (0x01 << MPU6050_INTERRUPT_FIFO_OFLOW_BIT)) || fifoCount >= 1024)
  {
    // reset so we can continue cleanly
    mpu.resetFIFO();
    fifoCount = mpu.getFIFOCount();
    Serial.println(F("FIFO overflow!"));

    // otherwise, check for DMP data ready interrupt (this should happen frequently)
  }
  else if (mpuIntStatus & (0x01 << MPU6050_INTERRUPT_DMP_INT_BIT))
  {
    // wait for correct available data length, should be a VERY short wait
    while (fifoCount < packetSize)
      fifoCount = mpu.getFIFOCount();

    // read a packet from FIFO
    mpu.getFIFOBytes(fifoBuffer, packetSize);

    // track FIFO count here in case there is > 1 packet available
    // (this lets us immediately read more without waiting for an interrupt)
    fifoCount -= packetSize;

    mpu.dmpGetQuaternion(&q, fifoBuffer);
    mpu.dmpGetGravity(&gravity, &q);
    mpu.dmpGetYawPitchRoll(ypr, &q, &gravity);
  }
}

void calibrateSetpoints(uint16_t repeatTimes = 400)
{
  digitalWrite(LED_PIN, HIGH);

  Serial << "Calibrating..." << endl;

  delay(2000);
  // reset interrupt flag and get INT_STATUS byte
  mpuInterrupt = false;
  mpuIntStatus = mpu.getIntStatus();

  mpu.resetFIFO();

  float input = 0;
  for (uint16_t i = 0; i < repeatTimes; i++)
  {
    mpuUpdate();
    input += ypr[BALANCING_AXIS] * 180 / M_PI;
  }
  angleSetpoint = input / repeatTimes;
  preference.putFloat(PK_ANGLE_SETPOINT, angleSetpoint);
  Serial << "New Angle Setpoint stored: " << angleSetpoint << endl;
  mpu.resetFIFO();
  digitalWrite(LED_PIN, LOW);
}

void setup()
{
  Serial.begin(115200);
  // Serial.print("setup() running on core ");
  // Serial.println(xPortGetCoreID());
  // delay(10000);
  Serial << "Booting..." << endl
         << endl;

  preference.setup();

  if (!wifi.connectToLastKnown())
  {
    wifi.robotAP();
  }

  // Start DNS server
  if (MDNS.begin(OTA_HOSTNAME))
  {
    Serial << "mDNS responder started, name: " << OTA_HOSTNAME << endl;
  }
  else
  {
    Serial << "Could not start MDNS responder" << endl;
  }

  ota.setup();
  operatingMode = STANDING_BY;

// join I2C bus (I2Cdev library doesn't do this automatically)
#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
  Wire.begin();
  Wire.setClock(400000); // 400kHz I2C clock. Comment this line if having compilation difficulties
#elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
  Fastwire::setup(400, true);
#endif

  // initialize device
  Serial.println(F("Initializing I2C devices..."));
  mpu.initialize();
  pinMode(MPU_INTERRUPT_PIN, INPUT);

  // verify connection
  Serial.println(F("Testing device connections..."));
  Serial.println(mpu.testConnection() ? F("MPU6050 connection successful") : F("MPU6050 connection failed"));

  // wait for ready
  // Serial.println(F("\nSend any character to begin DMP programming and demo: "));
  // while (Serial.available() && Serial.read())
  //   ; // empty buffer
  // while (!Serial.available())
  //   ; // wait for data
  // while (Serial.available() && Serial.read())
  //   ; // empty buffer again

  // load and configure the DMP
  Serial.println(F("Initializing DMP..."));
  devStatus = mpu.dmpInitialize();

  // supply your own gyro offsets here, scaled for min sensitivity
  mpu.setXGyroOffset(OFFSET_GYRO_X);
  mpu.setYGyroOffset(OFFSET_GYRO_Y);
  mpu.setZGyroOffset(OFFSET_GYRO_Z);
  mpu.setXAccelOffset(OFFSET_ACCEL_X);
  mpu.setYAccelOffset(OFFSET_ACCEL_Y);
  mpu.setZAccelOffset(OFFSET_ACCEL_Z);
  // make sure it worked (returns 0 if so)
  if (devStatus == 0)
  {
    // Calibration Time: generate offsets and calibrate our MPU6050
    mpu.CalibrateAccel(6);
    mpu.CalibrateGyro(6);
    Serial.println();
    mpu.PrintActiveOffsets();
    // turn on the DMP, now that it's ready
    Serial.println(F("Enabling DMP..."));
    mpu.setDMPEnabled(true);

    // enable Arduino interrupt detection
    Serial.print(F("Enabling interrupt detection (Arduino external interrupt "));
    Serial.print(digitalPinToInterrupt(MPU_INTERRUPT_PIN));
    Serial.println(F(")..."));
    attachInterrupt(digitalPinToInterrupt(MPU_INTERRUPT_PIN), dmpDataReady, RISING);
    mpuIntStatus = mpu.getIntStatus();

    // set our DMP Ready flag so the main loop() function knows it's okay to use it
    Serial.println(F("DMP ready! Waiting for first interrupt..."));
    dmpReady = true;

    // get expected DMP packet size for later comparison
    packetSize = mpu.dmpGetFIFOPacketSize();
  }
  else
  {
    // ERROR!
    // 1 = initial memory load failed
    // 2 = DMP configuration updates failed
    // (if it's going to break, usually the code will be 1)
    Serial.print(F("DMP Initialization failed (code "));
    Serial.print(devStatus);
    Serial.println(F(")"));
  }

  pinMode(LED_PIN, OUTPUT);
  pinMode(BUTTON1_PIN, INPUT);
  pinMode(BUTTON2_PIN, INPUT);

  xTaskCreatePinnedToCore(
      wirelessTask,   /* Function to implement the task */
      "wirelessTask", /* Name of the task */
      10000,          /* Stack size in words */
      NULL,           /* Task input parameter */
      1,              /* Priority of the task */
      NULL,           /* Task handle. */
      0);             /* Core where the task should run, main thread runs on core 1 */

  angleSetpoint = preference.getFloat(PK_ANGLE_SETPOINT, 0);
  Serial << "Angle Setpoint: " << angleSetpoint << endl;

  pidAngle.SetSampleTime(PID_SAMPLE_TIME); // 200 Hz
  pidAngle.SetOutputLimits(-STEPPER_MAX_SPEED, STEPPER_MAX_SPEED);
  pidAngle.SetMode(AUTOMATIC);

  stepper.setup();
  stepper.enableMotors();

  Serial << "Boot sucessful. Now running main loop..." << endl;
}

void loop()
{
  // Calibrate
  // Hold the robot up standing and keep it still until finish
  if (digitalRead(BUTTON1_PIN) == HIGH)
  {
    calibrateSetpoints();
  }

  mpuUpdate();
  angleInput = ypr[BALANCING_AXIS] * 180 / M_PI;
  pidAngle.Compute();
  stepper.leftSpeed(angleOutput);
  stepper.rightSpeed(angleOutput);
  // Serial.print(angleInput);
  // Serial.print("   ");
  // Serial.println(angleOutput);
}